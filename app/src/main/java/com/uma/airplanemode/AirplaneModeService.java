/**
 * Developed by the University of Málaga
 *
 * Gonzalo Chica Morales
 */

package com.uma.airplanemode;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import androidx.annotation.Nullable;
import android.util.Log;

import java.io.IOException;

public class AirplaneModeService extends Service {
    private static final String COMMAND_FLIGHT_MODE_1 = "settings put global airplane_mode_on";
    private static final String COMMAND_FLIGHT_MODE_2 = "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state";
    public final static String SWAP_AIRPLANE_MODE = "com.uma.airplanemode.SWAP";
    public final static String AUTO_AIRPLANE_MODE = "com.uma.airplanemode.AUTO";
    private boolean estado_anterior = false;
    private boolean estado_posterior = false;


    public AirplaneModeService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        int retry_times = 2;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            android.util.Log.d("Sleep Error 1", ex.toString());
        }
        estado_anterior = isFlightModeEnabled(getApplicationContext());
        Log.e("AirplaneMode","Estado anterior "+estado_anterior);

        switch (action) {
            case SWAP_AIRPLANE_MODE:
                setFlightMode(getApplicationContext());
                break;
            case AUTO_AIRPLANE_MODE:
                setFlightMode(getApplicationContext());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    android.util.Log.d("Sleep Error 2", ex.toString());
                }
                setFlightMode(getApplicationContext());
                break;
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            android.util.Log.d("Sleep Error 3", ex.toString());
        }
        estado_posterior = isFlightModeEnabled(getApplicationContext());

        Log.e("AirplaneMode","Estado final  "+estado_posterior);
        while( action==SWAP_AIRPLANE_MODE && estado_anterior==estado_posterior && retry_times>0){
            Log.e("AirplaneMode","Trying again "+retry_times+"more times."+estado_anterior+estado_posterior);
            setFlightMode(getApplicationContext());
            estado_posterior = isFlightModeEnabled(getApplicationContext());
            retry_times--;
        }
        return Service.START_NOT_STICKY;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static void setFlightMode(Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards.
            int enabled = isFlightModeEnabled(context) ? 0 : 1;
            // Set airplane / flight mode using "su" commands.
            String command = COMMAND_FLIGHT_MODE_1 + " " + enabled;
            executeCommandViaSu(context, "-c", command);
            command = COMMAND_FLIGHT_MODE_2 + " " + enabled;
            executeCommandViaSu(context, "-c", command);
        }
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private static boolean isFlightModeEnabled(Context context) {
        boolean mode = false;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards
            mode = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
        } else {
            // API 16 and earlier.
            mode = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
        }
        return mode;
    }

    private static void executeCommandViaSu(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i=0; i < 3; i++) {
            // "su" command executed successfully.
            if (success) {
                // Stop executing alternative su commands below.
                break;
            }
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // Execute command via "su".
                Runtime.getRuntime().exec(new String[]{su, option, command});
            } catch (IOException e) {
                success = false;
                Log.e("AirplaneModeService", "su command has failed due to: " + e.fillInStackTrace());
            } finally {
                success = true;
            }
        }
    }
}
