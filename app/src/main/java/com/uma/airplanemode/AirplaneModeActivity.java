/**
 * Developed by the University of Málaga
 *
 * Gonzalo Chica Morales
 */

package com.uma.airplanemode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

public class AirplaneModeActivity extends AppCompatActivity {
    private static final String COMMAND_FLIGHT_MODE_1 = "settings put global airplane_mode_on";
    private static final String COMMAND_FLIGHT_MODE_2 = "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state";
    Button swap, auto;
    private boolean estado_anterior = false;
    private boolean estado_posterior = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airplanemode);
        swap = (Button) findViewById(R.id.buttonSwap);
        auto = (Button) findViewById(R.id.buttonAuto);
        swap.setBackgroundColor(Color.parseColor("#40CAEE"));
        auto.setBackgroundColor(Color.parseColor("#40CAEE"));

        swap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                estado_anterior = isFlightModeEnabled(getApplicationContext());
                Log.e("AirplaneMode","Estado anterior "+estado_anterior);
                swap.setText("PROCESSING ...");
                auto.setEnabled(false);
                auto.setBackgroundColor(Color.parseColor("#73797F"));

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        int retry_times = 2;
                        swap(retry_times);
                    }
                }, 2000);
            }
        });

        auto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                estado_anterior = isFlightModeEnabled(getApplicationContext());
                Log.e("AirplaneMode","Estado anterior "+estado_anterior);
                changeButtons(1);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        auto();
                    }
                }, 2000);
            }
        });
    }
    public void changeButtons(int type){
        if(type == 0){
            if(isFlightModeEnabled(getApplicationContext())){
                swap.setText("Disable Airplane Mode");
                swap.setBackgroundColor(Color.parseColor("#b30000"));
            }else{
                swap.setText("Enable Airplane Mode");
                swap.setBackgroundColor(Color.parseColor("#009933"));

            }
            auto.setEnabled(true);
            auto.setBackgroundColor(Color.parseColor("#40CAEE"));
        }else if(type == 1){
            swap.setText("SWAP AIRPLANE MODE");
            swap.setBackgroundColor(Color.parseColor("#73797F"));
            swap.setEnabled(false);
            auto.setText("PROCESSING ...");
            auto.setBackgroundColor(Color.parseColor("#009933"));

        }else if(type == 2){
            swap.setEnabled(true);
            swap.setBackgroundColor(Color.parseColor("#40CAEE"));
            auto.setText("AUTO AIRPLANE MODE");
            auto.setBackgroundColor(Color.parseColor("#40CAEE"));
        }
    }

    public void swap(int retry_times){
        setFlightMode(getApplicationContext());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            android.util.Log.d("Sleep Error 1", ex.toString());
        }
        estado_posterior = isFlightModeEnabled(getApplicationContext());
        Log.e("AirplaneMode","Estado final "+estado_posterior);
        while(estado_anterior==estado_posterior && retry_times>0){
            Log.e("AirplaneMode","Trying again "+retry_times+"more times."+estado_anterior+estado_posterior);
            setFlightMode(getApplicationContext());
            estado_posterior = isFlightModeEnabled(getApplicationContext());
            retry_times--;
        }
        changeButtons(0);
    }
    public void auto(){
        setFlightMode(getApplicationContext());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            android.util.Log.d("Sleep Error 3sg", ex.toString());
        }

        setFlightMode(getApplicationContext());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            android.util.Log.d("Sleep Error 2sg", ex.toString());
        }

        estado_posterior = isFlightModeEnabled(getApplicationContext());
        Log.e("AirplaneMode","Estado final "+estado_posterior);
        changeButtons(2);
    }



    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public void setFlightMode(Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards.
            int enabled = isFlightModeEnabled(context) ? 0 : 1;
            // Set airplane / flight mode using "su" commands.
            String command = COMMAND_FLIGHT_MODE_1 + " " + enabled;
            executeCommandViaSu(context, "-c", command);
            command = COMMAND_FLIGHT_MODE_2 + " " + enabled;
            executeCommandViaSu(context, "-c", command);
        }
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private static boolean isFlightModeEnabled(Context context) {
        boolean mode = false;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards
            mode = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
        } else {
            // API 16 and earlier.
            mode = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
        }
        return mode;
    }

    private static void executeCommandViaSu(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i=0; i < 3; i++) {
            // "su" command executed successfully.
            if (success) {
                // Stop executing alternative su commands below.
                break;
            }
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // Execute command via "su".
                Runtime.getRuntime().exec(new String[]{su, option, command});
            } catch (IOException e) {
                success = false;
                Log.e("AirplaneModeService", "su command has failed due to: " + e.fillInStackTrace());
            } finally {
                success = true;
            }
        }
    }

}
