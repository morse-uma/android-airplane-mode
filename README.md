# Android Airplane Mode

This application acts as an airplane mode manager on Android devices. The application can be used directly from the user interface or remotely, making it ideal for automatic testing.

## Usage

The application can be used on the terminal with the provided user interface, or remotely through adb commands.

### Permissions

The first time the application is used (through the device interface) the user will need to accept the super user mode for being able to use the application.

Once allowed, the application can be re-opened or used through adb.

### Adb commands

The application can be used remotely sending intents to the service via the startservice command of adb:

`adb shell am startservice -n com.uma. airplanemode/.AirplaneModeService`

The application accepts the following intents (-a):

`com.uma.airplanemode.SWAP`

`com.uma.airplanemode.AUTO`

#### Examples

`adb shell am startservice -n com.utils.airplanemode/.AirplaneModeService -a com.utils.airplanemode.SWAP`

`adb shell am startservice -n com.utils.airplanemode/.AirplaneModeService -a com.utils.airplanemode.AUTO`

### Device Interface

The application can be used through the device interface. The application can be used in two different ways: Swap, that enables or disables the Airplane mode, depending on the current status, and Auto, that performs two consecutive swaps, enabling and disabling airplane mode or vice versa. 

## Authors

* Gonzalo Chica Morales

## License

Copyright 2020 MORSE Research Group - University of Malaga

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
